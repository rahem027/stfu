//
// Created by hemil on 11/09/21.
//
#define STFU_IMPL
#include <stfu/stfu.h>


int main() {
    stfu::test("expect no throws demo", [] {
        std::vector<int> v = {1, 2, 3, 4};

        stfu::test("accessing first element of non empty vector should work", [&] {
            expect_no_exception([&] {
                expect(v.at(1) == 2);
            });
        });

        stfu::test("accessing v.at(v.size()) throws std::out_of_range causing expect_no_exception to fail", [&] {
            expect_no_exception([&] {
                int garbage_value = -1;
                expect(v.at(v.size()) == garbage_value);
            });
        });
    });
}
